# jquiz-ai-poc

This project is a Proof Of Concept for using LLM to generate quiz questions.

I speak about it in the following article : [LLM & Quiz with Ollama, Quarkus & LangChain4j](https://david.le-montagner.fr/posts/llm-et-quiz-avec-ollama-quarkus-et-langchain4j/). Sorry, it's only available in French for the moment.

---

Ce projet est un Proof Of Concept sur l'utilisation d'un LLM pour générer des questions de quiz.

Je parle de ce sujet dans l'article suivant : [LLM & Quiz avec Ollama, Quarkus & LangChain4j](https://david.le-montagner.fr/posts/llm-et-quiz-avec-ollama-quarkus-et-langchain4j/). 
