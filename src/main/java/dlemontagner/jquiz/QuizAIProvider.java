package dlemontagner.jquiz;

import dev.langchain4j.service.MemoryId;
import dev.langchain4j.service.SystemMessage;
import dev.langchain4j.service.UserMessage;
import io.quarkiverse.langchain4j.RegisterAiService;

@RegisterAiService
public interface QuizAIProvider {
    @SystemMessage("You are a quiz questions provider")
    @UserMessage(
            """
            Provide a question about ${theme}.
            You will provide one question and four answers about this theme.
            The theme should not be the answer as participants will know about the theme.
            Only one of these answers is correct.
            If you can't understand the theme or if you don't have any questions about it use the theme "general knowledge" instead of the given theme.
            Never return the same question twice.
            You must respond in a valid JSON format.
            You must not wrap JSON response in backticks, markdown, or in any other way, but return it as plain text.
            """)
    QuizAIQuestion newQuestion(@MemoryId int id, String theme);
}
