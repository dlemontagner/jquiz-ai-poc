package dlemontagner.jquiz;

import java.util.List;

public record QuizAIQuestion(String question, List<QuizAIAnswer> answers) {
}
