package dlemontagner.jquiz;

public record QuizAIAnswer(String text, boolean correct) {
}
