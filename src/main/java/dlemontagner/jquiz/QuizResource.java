package dlemontagner.jquiz;

import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/quiz")
public class QuizResource {
    @Inject
    QuizAIProvider quizAIProvider;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public QuizAIQuestion newQuestion(String theme) {
        return quizAIProvider.newQuestion(1, theme);
    }
}
